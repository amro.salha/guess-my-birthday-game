from random import randint

name = input("Hi! What is your name? ")

for loop in range(5):
    guess_year = randint(1924, 2004)
    guess_month = randint(1, 12)
    print(name, "Were you born in ", guess_month, "/", guess_year, "? ")
    answer = input("Yes or No? ")
    if answer == "Yes":
        print("I knew it!")
        break
    elif loop < 4 and answer == "No":
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
        exit()
